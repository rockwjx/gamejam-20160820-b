﻿using UnityEngine;
using System.Collections;

public class ColliderAnchor : MonoBehaviour {

	[SerializeField]
	private GameObject floor;

	[SerializeField]
	private BoxCollider colliderTop;
	[SerializeField]
	private BoxCollider colliderBottom;
	[SerializeField]
	private BoxCollider colliderLeft;
	[SerializeField]
	private BoxCollider colliderRight;

	private void Start()
	{
		float xScale = floor.transform.localScale.x + 0.1f;
		float zScale = floor.transform.localScale.z + 0.1f;
		colliderTop.transform.localPosition = new Vector3(0, 0, zScale * 5);
		colliderTop.size = new Vector3(xScale * 10, 1, 1);

		colliderBottom.transform.localPosition = new Vector3(0, 0, -zScale * 5);
		colliderBottom.size = new Vector3(xScale * 10, 1, 1);

		colliderLeft.transform.localPosition = new Vector3(-xScale * 5, 0, 0);
		colliderLeft.size = new Vector3(1, 1, zScale * 10);

		colliderRight.transform.localPosition = new Vector3(xScale * 5, 0, 0);
		colliderRight.size = new Vector3(1, 1, zScale * 10);
	}
}
