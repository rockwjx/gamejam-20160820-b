﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour {

//	public KeyCode up;
//	public KeyCode down;
//	public KeyCode left;
//	public KeyCode right;
	[SerializeField]
	private float _speed = 0.1f;

	[SerializeField]
	private float _jumpFactor = 2f;

	private CharacterController _moveController;
	private Vector3 _move;
	private Rigidbody _rigid;

	// Use this for initialization
	void Start () {
		_moveController = GetComponent<CharacterController>();
		_rigid = GetComponent<Rigidbody>();
	}
	
	private void Update()
	{
		// Get the axis and jump input.
		float h = Input.GetAxisRaw("Horizontal");
		float v = Input.GetAxisRaw("Vertical");

		_move = (v * Vector3.forward + h * Vector3.right).normalized;

		if (Input.GetKey(KeyCode.LeftShift))
		{
			_move *= 0.5f;
		}
		if (Input.GetKey(KeyCode.Z))
		{
			_move *= 2f;
		}
		_move *= _speed;
//		if (Input.GetKey(KeyCode.Space))
//		{
//			if (_moveController.isGrounded)
//				_move.y = _jumpFactor;
//		}
//		_move.y -= 10.0f * Time.deltaTime;
	}

	private void FixedUpdate()
	{
		
		Assert.IsNotNull<CharacterController>(_moveController);
		if (_move.magnitude > 0.001f)
		{
			_move.y = 0;
			_moveController.Move(_move);
		}
	}
}
