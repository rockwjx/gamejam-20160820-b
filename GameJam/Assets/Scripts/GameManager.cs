﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	public static GameManager instance = null;

	[SerializeField]
	private GameObject playerPrefab = null;
	[SerializeField]
	private GameObject virusPrefab = null;

	public int maxVirus = Const.EASY_COUNT;
	public int currentVirus = 0;
	public int playerHp = 1;

	private GameMode _mode = GameMode.Easy;
	public GameMode gameMode {
		get
		{
			return _mode;
		}
		private set
		{
			_mode = value;
		}
	}


	private void Awake()
	{
		instance = this;
		DontDestroyOnLoad(this.gameObject);
	}

	public void InitGame()
	{
		Debug.Log("Max: "+maxVirus);
		PoolManager.Instance.CreatePool(playerPrefab, 1);
		PoolManager.Instance.CreatePool(virusPrefab, Const.EASY_COUNT, Const.HARD_COUNT);
		GameManager.instance.LoadScene(SceneName.Game);
	}

	public void StartGame()
	{
		GameScene.instance.SetGameState(GameState.GameReady); 
	}

	private void Update()
	{
		if (Input.GetKeyUp(KeyCode.R))
		{
			LoadScene(SceneName.Game);
		}
	}
		
	public void LoadScene(SceneName scene)
	{
		switch (scene)
		{
			case SceneName.Title:
				SceneManager.LoadScene(0, LoadSceneMode.Single);
				break;
			case SceneName.Game:
				SceneManager.LoadScene(1, LoadSceneMode.Single);
				break;
		}
	}

	public void SetMode(GameMode mode)
	{
		gameMode = mode;
	}
}
