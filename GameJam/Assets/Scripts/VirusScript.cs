﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
public class VirusScript : MonoBehaviour {
    
    [SerializeField]
    private float _speed = 1;
	[SerializeField]
	private bool _canDivide;
	[SerializeField]
	private Vector3 _direction;
	[SerializeField]
	private int _hp = -1;

    private Direction _wallDir = Direction.Right;
	private Vector3 _lastPoint;
    
    #region PublicMethods
	public void Init(Vector3 dir, float speed = 4, bool divide = true, int hp = -1)
    {
		_lastPoint = transform.position;
		_speed = speed;
		_canDivide = divide;
		_hp = hp;
		_direction = dir;
		GetComponent<Rigidbody>().velocity = _direction.normalized * _speed;
		GetComponent<Collider>().enabled = true;
		GameManager.instance.currentVirus++;
    }
    #endregion

    #region EventHandler
    void OnCollisionEnter(Collision col)
    {
		if (_hp > 0)
		{
			_hp--;
			if (_hp == 0)
			{
				Death();
				return;
			}
		}

        string collisionName = col.gameObject.name;
        if (collisionName == "Bottom")
        {
            _wallDir = Direction.Up;
        }
        else if (collisionName == "Top")
        {
            _wallDir = Direction.Down;
        }
        else if (collisionName == "Right")
        {
            _wallDir = Direction.Left;
        }
        else if (collisionName == "Left")
        {
            _wallDir = Direction.Right;
        }
        else
        {
            return;
        }
        SetDirection(_wallDir);
    }

    void OnCollisionExit (Collision col)
    {
        if (col.gameObject.layer != LayerMask.NameToLayer("Wall"))
        {
            return;
        }

		if (GameManager.instance.currentVirus < GameManager.instance.maxVirus)
        {
			if (_canDivide)
          	  Duplicate();
        }
    }

	public void Death()
	{
		GetComponent<Collider>().enabled = false;
		GetComponent<Rigidbody>().velocity = Vector3.zero;
		PoolManager.Instance.ReleaseObject(this.gameObject);
		//				Debug.Log("----------Death----------");

		if (--GameManager.instance.currentVirus <= 0)
		{
			GameScene.instance.EndGame();
		}
	}
    #endregion

	#region PrivateMethods
	private void Duplicate()
	{
		GameObject go = PoolManager.Instance.GetObject(Const.VIRUS_STR);
		if (go == null)
		{
			return;
		}

		Vector3 pos = this.transform.position;
		go.transform.position = pos;
		go.transform.localScale = this.transform.localScale;

		float speed = Random.Range(3, 7);
		int hp = (Vector3.Distance(pos, _lastPoint) >= 2) ? Const.VIRUS_BASE_HP : 2;
		bool divide = (GameManager.instance.currentVirus > GameManager.instance.maxVirus / 2) ? false : true;

		Vector3 speedDir = _direction;

		switch (_wallDir)
		{
			case Direction.Up:
				speedDir.x = -speedDir.x;
				break;
			case Direction.Down:
				speedDir.x = -speedDir.x;
				break;
			case Direction.Left:
				speedDir.z = -speedDir.z;
				break;
			case Direction.Right:
				speedDir.z = -speedDir.z;
				break;
			default:
				break;
		}
		float angle = Random.Range(-40f, 40f);
		speedDir = Quaternion.AngleAxis(angle, Vector2.up) * speedDir;
		go.GetComponent<VirusScript>().Init(speedDir, speed, divide, hp);
	}
	#endregion

	#region DirectionController
	private void SetDirection(Direction direction)
	{
		float directionValue1 = Random.Range(0.5f, 1f);
		float directionValue2 = Random.Range(0.3f, 0.7f);

		switch (direction)
		{
			case Direction.Up:
				directionValue1 *= Random.Range(0, 2) == 1 ? -1 : 1;
				break;
			case Direction.Down:
				directionValue1 *= Random.Range(0, 2) == 1 ? -1 : 1;
				directionValue2 = -directionValue2;
				break;
			case Direction.Left:
				directionValue2 *= Random.Range(0, 2) == 1 ? -1 : 1;
				directionValue1 = -directionValue1;
				break;
			case Direction.Right:
				directionValue2 *= Random.Range(0, 2) == 1 ? -1 : 1;
				break;
			default:
				break;
		}
		_direction = new Vector3(directionValue1, 0, directionValue2).normalized;
		GetComponent<Rigidbody>().velocity = _direction * _speed;
	}
	#endregion
}
