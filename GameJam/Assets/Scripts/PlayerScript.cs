﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
public class PlayerScript : MonoBehaviour {

	[SerializeField]
	private float _hp = 1;
	public float Life
	{
		get
		{
			return _hp;
		}
	}

	public void Init()
	{
		Debug.Log("Init");
		transform.position = new Vector3(-3.4f, 0.15f, 4.3f);
		_hp = GameManager.instance.playerHp;
		ResetEffect();
	}

	private void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.layer == LayerMask.NameToLayer("Virus")
		    && col.gameObject.activeInHierarchy)
		{
			_hp--;
			Debug.Log(col.gameObject.name+"------HIT------"+ _hp);
			if (_hp == 0)
			{
				GameScene.instance.EndGame();
			}
			else
			{
				col.gameObject.GetComponent<VirusScript>().Death();
				GetComponent<MeshRenderer>().material.color = Color.red;
				GetComponent<Collider>().enabled = false;
				CancelInvoke("ResetEffect");
				Invoke("ResetEffect", 0.5f);
			}
		}
	}

	private void ResetEffect()
	{
		GetComponent<MeshRenderer>().material.color = Color.white;
		GetComponent<Collider>().enabled = true;
	}

	public void LifeUp(int life = 1)
	{
		_hp += life;
	}
}
