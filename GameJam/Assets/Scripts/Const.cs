﻿public enum GameMode
{
	Easy,
	Normal,
	Hard,
}

public enum SceneName
{
	Title,
	Game,
}

public enum Direction
{
	Up,
	Down,
	Left,
	Right,
}

public enum GameState
{
	None = -1,
	BeforeGame = 0,
	GameReady,
	Gaming,
	GamePause,
	GameOver,
	Restart,
	Exit,
}

public class Const
{
	public const string PLAYER_STR = "Player";
	public const string VIRUS_STR = "Virus";

	public const int EASY_COUNT = 10;
	public const int NORMAL_COUNT = 25;
	public const int HARD_COUNT = 50;

	public const int VIRUS_BASE_HP = 15;

	public const int PLAYER_COUNT = 1;

	public const float LIFE_UP_TIME = 25;
}