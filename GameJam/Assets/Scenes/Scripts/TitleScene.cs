﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TitleScene : MonoBehaviour {

	public void OnModeChange(int index)
	{
		GameManager.instance.SetMode((GameMode)index);
	}

	public void GameStart()
	{
		GameManager.instance.InitGame();
		Application.targetFrameRate = 60;
	}
}
