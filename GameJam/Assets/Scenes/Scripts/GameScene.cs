﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameScene : MonoBehaviour {

    [SerializeField]
	private Text timerLabel = null;
	[SerializeField]
	private Text playerLifeLabel = null;
	[SerializeField]
	private Animation lifeUpAnim = null;
    [SerializeField]
    private GameObject gameOverDialog = null;

	public static GameScene instance = null;
	public float timer
	{
		get;
		set;
	}
	public float lifeUpTimer = Const.LIFE_UP_TIME;

	private GameState _state;
	private GameState _nextState;
	private PlayerScript[] _players = new PlayerScript[Const.PLAYER_COUNT];

    
	void Awake()
	{
		instance = this;
		_nextState = GameState.BeforeGame;
	}

    void Start()
    {
		gameOverDialog.SetActive(false);
		GameManager.instance.currentVirus = 0;
		GameManager.instance.StartGame();
	}

	private void Update()
	{
		if (_nextState != GameState.None)
		{
			Debug.Log("next:"+_nextState);
			switch (_nextState)
			{
				case GameState.GameReady:
					StartGame();
					break;
				case GameState.Gaming:
					break;
				case GameState.GameOver:
					break;
				case GameState.Restart:
					break;
			}
			_state = _nextState;
			_nextState = GameState.None;
		}
		if (_state != GameState.None)
		{
			switch (_state)
			{
				case GameState.Gaming:
					timer += Time.deltaTime;
					lifeUpTimer -= Time.deltaTime;
					if (lifeUpTimer <= 0)
					{
						Player1Up();
						lifeUpAnim.Play();
						lifeUpTimer = Const.LIFE_UP_TIME;
					}
					playerLifeLabel.text = "Life: " + _players[0].Life;
					timerLabel.text = string.Format("Time: {0:0.00}", timer);

					switch (GameManager.instance.maxVirus)
					{
						case Const.EASY_COUNT:
							if (GameScene.instance.timer >= 17)
							{
								GameManager.instance.maxVirus = Const.NORMAL_COUNT;
								Debug.Log(GameScene.instance.timer+" Max: "+GameManager.instance.maxVirus);
							}
							break;
						case Const.NORMAL_COUNT:
							if (GameScene.instance.timer >= 36)
							{
								GameManager.instance.maxVirus = Const.HARD_COUNT;
								Debug.Log(GameScene.instance.timer+" Max: "+GameManager.instance.maxVirus);
							}
							break;
						case Const.HARD_COUNT:
							break;
					}
					break;
			}
		}
	}

    #region PublicMethods
    public void StartGame()
    {
        Debug.Log("-- StartGame --");
		GameObject go = PoolManager.Instance.GetObject(Const.VIRUS_STR);
		GameObject playerGo = PoolManager.Instance.GetObject(Const.PLAYER_STR);
		_players[0] = playerGo.GetComponent<PlayerScript>();
		_players[0].Init();

		float x = Random.Range(-5.0f, 5.0f);
		float z = Random.Range(-4.0f, 4.0f);
		go.GetComponent<VirusScript>().Init(new Vector3(x, 0, z));

		lifeUpTimer = Const.LIFE_UP_TIME;
		timer = 0f;
		lifeUpAnim.GetComponent<Text>().enabled = false;
		GameManager.instance.maxVirus = Const.EASY_COUNT;
		_nextState = GameState.Gaming;
    }

    public void EndGame()
    {
        Debug.Log("-- EndGame --");

		_state = GameState.GameOver;
        gameOverDialog.SetActive(true);
        Time.timeScale = 0;
    }
    
    public void BackToMenu()
    {
        Time.timeScale = 1;
		ResetPool();
		GameManager.instance.LoadScene(SceneName.Title);
    }

    public void Retry()
    {
        Time.timeScale = 1;
		ResetPool();
		GameManager.instance.LoadScene(SceneName.Game);
    }

	public void SetGameState(GameState state)
	{
		_nextState = state;
	}
    #endregion
	private void ResetPool()
	{
		PoolManager.Instance.ResetPool(Const.VIRUS_STR);
		PoolManager.Instance.ResetPool(Const.PLAYER_STR);
//		PoolManager.Instance.ShrinkPool(Const.VIRUS_STR);
//		PoolManager.Instance.ShrinkPool(Const.PLAYER_STR);
	}

	private void Player1Up()
	{
		foreach (var player in _players)
		{
			player.LifeUp();
			Debug.Log("1UP! "+player.Life);
		}
	}
}
