﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class SceneLoader {

	[MenuItem("Test/Load Title &#l")]
	public static void LoadTitle()
	{
		EditorSceneManager.OpenScene("Assets/Scenes/Title.unity",OpenSceneMode.Single);
		EditorApplication.isPlaying = true;
	}
}
